#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_PN532.h>
LiquidCrystal_I2C lcd(0x3f,16,2);
#define PN532_SCK  (2)
#define PN532_MOSI (3)
#define PN532_SS   (4)
#define PN532_MISO (5)
Adafruit_PN532 nfc(PN532_SCK, PN532_MISO, PN532_MOSI, PN532_SS);
//connetcing map
//PN532
//SPI connecting
//MOSI to 3 (digital)
//SS to 4 (digital)
//MISO to 5 (digital)
//SKC to 2 (digital)
//LCD 16/2 with I2C
//SDA to A5
//SCL to A4

void setup(void) {
  lcd.init();                      
  lcd.backlight();
  Serial.begin(115200);
  Serial.println("Hello!");
  lcd.setCursor(0,0);
  lcd.print("Hello!");
  lcd.setCursor(0,1);
  lcd.print("By Kalayda P.");
  nfc.begin();
  delay(1000);
  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    lcd.clear();
    lcd.print("Didnt't find PN53x Board");
    while (1); // halt
  }
  Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX); 
  Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC); 
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
  nfc.SAMConfig();
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("PN53x Board OK");
  delay(2000);
  nfc.SAMConfig();
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Waiting card...");
  Serial.println("Waiting for an ISO14443A Card ...");
}


void loop(void) {
  uint8_t success;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };
  uint8_t uidLength;                      
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
  if (success) {
    // Display some basic information about the card
    Serial.println("Found an ISO14443A card");
    Serial.print("  UID Length: ");
    Serial.print(uidLength, DEC);
    Serial.println(" bytes");
    Serial.print("  UID Value: ");
    nfc.PrintHex(uid, uidLength);
    
    if (uidLength == 4)
    {
      // We probably have a Mifare Classic card ... 
      uint32_t cardid = uid[0];
      cardid <<= 8;
      cardid |= uid[1];
      cardid <<= 8;
      cardid |= uid[2];  
      cardid <<= 8;
      cardid |= uid[3]; 
      Serial.print("Seems to be a Mifare Classic card #");
      Serial.println(cardid);
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Card");
      lcd.setCursor(5,0);
      lcd.print(cardid);
      lcd.setCursor(0,1);
      lcd.print ("Door open");
      delay(3000);
      lcd.clear();
      lcd.print("Waiting card...");
    }
    Serial.println("");
    
  }
  
}

